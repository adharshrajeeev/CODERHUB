export const baseUserUrl = "http://localhost:7000/api/"
// export const baseAdminUrl = "http://localhost:7000/api/admin"

//USER SIDE URLS===>>>>>>>

export const SIGNUP='/signup'
export const USER_LOGIN='/login'
export const ADD_POST='/addPosts'
export const SHOW_USERS='/users'
export const GET_USER_DETAILS='/userDetails'
export const GET_PROFILE_DETAILS='/userProfileDetails'
export const SHOW_USER_POST='/userPosts'
export const EXPLORE_ALLPOST='/explore'
export const  ALL_POSTS='/posts'    
export const FOLLOW_USER='/follow'
export const UNFOLLOW_USER='/unFollow'
export const REMOVE_FOLLOWER='/removeFollower'
export const ADD_USER_BIO='/addBio'
export const LIKE_POST='/like'
export const UNLIKE_POST='/unLike'
export const DELETE_POSTS='/deletePost'
export const GET_LIKCOUNT='/likeCount'
export const ADD_PROFILEIMAGE='/profilePicture'
export const ADD_COVERPICTURE='/coverPicture'
export const GET_LIKE_STATUS='/likeStatus'
export const ADD_COMMENTS='/addComment'
export const GET_EDITPOST_DETAILS='/editPost'
export const GET_CONNECTIONS='/connections'
export const GET_FOLLOWING_LIST='/followings'
export const GET_FOLLOWERS_LIST='/followers'
export const GET_USER_SUGGESTIONS='/suggestionUsers';
export const REPORT_POST='/reportPost'
export const REPORT_POST_HOME='/reportPostHome'
export const UPDATE_USER_POST='/updatePost'
export const DELETE_COMMENT='/deleteComment'
export const GET_USERDATA='/getUserData'
export const UPDATE_USER_DETAILS='/updateUserDetails'
export const CHANGE_USER_PASSWORD='/changePassword'
export const SEND_OTP_REQUEST='/sendOtp'
export const OTP_AND_RESET_PASS='/resetPassword'
export const EMAIL_VERIFICATION_SIGNUP='/signupVerification'
export const OTP_SIGNUP='/otpVerification'

export const GET_USER='/user'

export const ADD_NEW_CONVERSATION='/conversation'
export const GET_ALL_CONVERSATIONS='/allConversation'
export const GET_USER_MESSAGES='/allMessages'

export const SEND_NEW_MESSAGE='/messages'


export const SEARCH_USER_FOLLOWINGS='/searchUsers'
export const SEARCH_ALL_USERS='/searchAll'
export const GET_ALL_NOTIFICATIONS='/notifications'
export const GET_NOTIFICATION_DATA='/notificationDetails'
export const CHANGE_NOTIFICATION_STATUS='/readNotification'
export const DELETE_NOTIFICATION='/deleteNotification'






//ADMIN SIDE URLS===>>>>>>

export const ADMINLOGIN='/admin/login'
export const GET_ALL_USERS='/admin/allUsers'
export const CHANGE_USER_STATUS='/admin/changeStatus'
export const GET_REPORTED_POSTS='/admin/reportedPosts'
export const CHANGE_POST_STATUS='/admin/postStatus'
export const GET_ALL_POSTS='/admin/allPosts'
export const USER_MONTH_WISE_GROWTH='/admin/monthWiseUser'
export const POST_MONTH_WISE_COUNT='/admin/monthWisePostCount'
export const GET_POST_DETAILS='/admin/postDetails'